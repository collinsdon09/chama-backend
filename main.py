from typing import List
from fastapi import FastAPI
from pydantic import BaseModel
from pymongo import MongoClient, ASCENDING
from pymongo.results import DeleteResult
from fastapi.middleware.cors import CORSMiddleware
from bson import ObjectId
# import websocket
import asyncio
from pymongo.errors import DuplicateKeyError
from fastapi import Query
from fastapi.responses import JSONResponse


app = FastAPI()
origins = [

    "http://localhost:5173",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


client = MongoClient("mongodb://localhost:27017")
db = client["chama"]
members_collection = db['members']


class Member(BaseModel):
    name: str
    ID: str
    DOB: str
    member_number: str
    Location: str
    Savings: list
    Loans: list


@app.post("/save-member")
async def save_receipt(member: Member):
    # Connect to MongoDB
    print(member)

    # Save the receipt object to MongoDB
    member_data = member.dict()
    print("saved:", members_collection.insert_one(member_data))

    # # Close the MongoDB connection
    # client.close()

    return {"message": "Member saved successfully"}


@app.get("/get-member/{member_number}")
def get_member(member_number: str):
    # Find the document with the specified receipt_number
    document = members_collection.find_one(
        {"member_number": str(member_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        # //return document
        return JSONResponse(content=document)
    else:
        # return {"message": "Document not found"}
        return JSONResponse(content={"message": "Document not found"})


@app.get("/get-members")  # get receipts by their status
async def get_members():
    query = {}

    members = members_collection.find(query).sort("created_at", ASCENDING)
    serialized_members = []

    seen_ids = set()

    for member in members:
        receipt_id = str(member["_id"])

        if receipt_id not in seen_ids:
            member["_id"] = str(member["_id"])
            serialized_members.insert(0, member)
            seen_ids.add(receipt_id)

    return serialized_members



@app.delete("/delete_member/{member_number}")
async def delete_document(member_number: str):
    # Find the document by its ID
    document = members_collection.find_one({"member_number": member_number})

    if document:
        # Delete the document
        members_collection.delete_one({"member_number": member_number})
        return {"message": "member deleted successfully."}
    else:
        return {"message": "member not found."}



# class Receipt3(BaseModel):
#     receipt_number: str
#     note: str
#     status: str
#     receipt_type: str
#     refund_for: str
#     order: str
#     created_at: str
#     updated_at: str
#     source: str
#     receipt_date: str
#     cancelled_at: str
#     total_money: float
#     total_tax: float
#     points_earned: float
#     points_deducted: float
#     points_balance: float
#     customer_id: str
#     total_discount: float
#     employee_id: str
#     store_id: str
#     pos_device_id: str
#     dining_option: str
#     total_discounts: list
#     total_taxes: list
#     tip: float
#     surcharge: float
#     line_items: list
#     cart_items: list


# @app.post("/save-receipt-final")
# async def save_receipt(receipt: Receipt3):
#     # Connect to MongoDB
#     print(receipt)

#     # Save the receipt object to MongoDB
#     receipt_data = receipt.dict()
#     print("saved:", sales_test_col.insert_one(receipt_data))

#     # # Close the MongoDB connection
#     # client.close()

#     return {"message": "Receipt saved successfully"}


# @app.put("/update-documents")
# def update_documents():
#     # Update operation
#     result = sales_collection.update_many({}, {"$set": {"missing_items": []}})
#     updated_count = result.modified_count

#     return {"message": f"Updated {updated_count} documents."}
